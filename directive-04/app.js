'use strict';

angular.module('directiveApp', [])

  .controller('RatingCtrl', function () {
    this.restaurant = {
      personal: 2,
      food: 3,
      place: 4
    };
  })

  .directive("rating", function () {
    // TODO 4.1 - přidejte závislost na averageRating

    return {
      scope: {
        rating: "="
      },
      restrict: "A",
      templateUrl: "rating.html",
      transclude: true,

      // TODO 4.2 - komunikace s averageRating
      link: function (scope, element, attr) {

        scope.hasStar = function (starNumber) {
          return starNumber <= scope.rating;
        };
      }
    };
  })

  .directive("averageRating", function () {

    // TODO 3.1 - použití controlleru
    // TODO 2.2 - aktivace transkluze
    return {
      restrict: "A",
      templateUrl: "average-rating.html"
    };

    function averageController() {
      var count = 0;
      var sum = 0;

      this.addRating = function (rating) {
        count++;
        sum += parseInt(rating);
      };

      this.getAverage = function () {
        if (count === 0) {
          return 0;
        }

        return Math.round((sum / (count * 5)) * 100);
      };
    }

  });