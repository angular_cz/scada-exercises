angular.module('directiveApp', [])
    .controller('InfoController', function() {
      this.reportChange = function(tabHeader) {
        this.currentTabHeader = tabHeader;

        $log.info('Tab changed:', tabHeader);
      }
    })
    .directive('tab', function tabDirective() {

      // TODO 1.1 - doplňte atribut pro transcluzi
      // TODO 3.1 - přidejte závislost na tabset
      return {
        templateUrl: 'tab.html',
        scope: {
          header: '@'
        },

        // TODO 3.2 - komunikace s direktivou tabset
        link: function(scope, element, attrs) {

        }
      }
    })

    .directive('tabset', function () {

      function tabsetDirectiveController($log, $scope) {

        this.tabs = [];

        this.add = function(tab) {
          $log.info('Tab has been added:', tab.header);

          this.tabs.push(tab);
          this.hide(tab);

          if (this.tabs.length == 1) {
            this.select(tab);
          }
        };

        this.select = function(tab) {
          this.tabs.map(this.hide);

          tab.active = true;

        };

        this.hide = function(tab) {
          tab.active = false;
        };
      }

      // TODO 2.2 - přidejte direktivě controller
      return {
        templateUrl: 'tabset.html',
        transclude:true,
        scope: {}
      };
    });



