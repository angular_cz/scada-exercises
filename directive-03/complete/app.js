angular.module('directiveApp', [])
    .controller('InfoController', function() {
      this.reportChange = function(tabHeader) {
        this.currentTabHeader = tabHeader;

        $log.info('Tab changed:', tabHeader);
      }
    })

    .directive('tab', function tabDirective() {
      return {
        require: '^tabset',
        templateUrl: 'tab.html',
        scope: {
          header: '@'
        },
        transclude: true,

        link: function(scope, element, attrs, tabsetController) {
          tabsetController.add(scope);
        }
      }
    })

    .directive('tabset', function() {

      function tabsetDirectiveController($log) {
        this.tabs = [];
        this.add = function(tab) {
          $log.info('Tab has been added:', tab.header);

          this.tabs.push(tab);
          this.hide(tab);

          if (this.tabs.length == 1) {
            this.select(tab);
          }
        };

        this.select = function(tab) {
          this.tabs.map(this.hide);

          tab.active = true;
          this.onChange({header: tab.header});
        };

        this.hide = function(tab) {
          tab.active = false;
        };
      }

      return {
        templateUrl: 'tabset.html',
        transclude: true,
        scope: {},
        bindToController: {
          onChange: "&"
        },
        controller: tabsetDirectiveController,
        controllerAs: 'tabset'
      };
    });



