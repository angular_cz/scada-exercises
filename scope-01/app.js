angular.module('moduleExample', [])
    .controller('UserController', function($scope, $rootScope) {
      this.user = {
        name: 'Petr',
        surname: 'Novák',
        yearOfBirth: 1982
      };

      // TODO 4.1 - napište watch sledující změnu yearOfBirth
      // TODO 4.2 - přepište expression na funkci
      // TODO 5.1 - vyšlete zprávu yearOfBirth:changed
      // TODO 6.2 - odesílejte zprávu na $rootScope

      this.getAge = function() {
        // TODO 2 - odkomentujte
        //console.log('getAge');
        var now = new Date();
        return now.getFullYear() - this.user.yearOfBirth;
      };

      this.clear = function() {
        this.user = {};
      };
    })
    .controller('CountController', function($scope) {
      this.count = 0;

      // TODO 5.2 - poslouchejte zprávu yearOfBirth:changed
    });
