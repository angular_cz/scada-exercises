angular.module('FormApp', ['ngMessages', 'ngStorage'])
  .controller('UserDetailController', function(userStorage) {
    this.userStorage = userStorage;

    this.clear = function(userForm) {
      userStorage.clear();
      this.reset(userForm);
    };

    this.save = function(userForm) {
      if (userForm.$invalid) {
        return;
      }

      userStorage.save(this.user);
    };

    this.reset = function(userForm) {
      this.user = userStorage.get();
      userForm.$setPristine();
      userForm.$setUntouched();
    };

    this.user = userStorage.get();
  })
  
  .directive('validateCzechPhoneNumber', function() {
    var pattern = /^(\+420)?( ?\d{3}){3}$/;

    return {
      restrict: 'A',
      require: 'ngModel',
      link: function(scope, element, attrs, ngModelController) {
        ngModelController.$validators.czechPhoneNumber = function(value) {
          if (ngModelController.$isEmpty(value)) {
            return true;
          }

          return pattern.test(value);
        };
      }
    };
  })
  
  .service('userStorage', function($localStorage) {
    this.userStorage = $localStorage.$default({
      user: {
        name: 'unknown name'
      }
    });

    this.get = function() {
      return angular.copy(this.userStorage.user);
    };

    this.save = function(user) {
      this.userStorage.user = angular.copy(user);
    };

    this.clear = function() {
      this.userStorage.$reset({
        user: {}
      });
    };

  });