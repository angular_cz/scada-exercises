'use strict';

angular.module('filterApp', [])
    .controller('FilterCtrl', function() {
      this.inputText = "Ahoj světe ...";

      <!--TODO 5.1a - filtrujte tickety podle stavu-->
      this.search = {
      };

      this.todos = [
        {status: "open", name: "TODO 03", owner: "Petr", duedate: '2015-10-03T00:00:00+01:00'},
        {status: "open", name: "TODO 09", owner: "Pavel", duedate: '2015-10-11T00:00:00+01:00'},
        {status: "open", name: "TODO 04", owner: "Petr", duedate: '2015-10-03T12:00:00+01:00'},
        {status: "open", name: "TODO 07", owner: "Pavel", duedate: '2015-10-06T12:00:00+01:00'},
        {status: "open", name: "TODO 05", owner: "Petr", duedate: '2015-10-05T00:00:00+01:00'},
        {status: "open", name: "TODO 02", owner: "Petr", duedate: '2015-10-02T12:00:00+01:00'},
        {status: "open", name: "TODO 01", owner: "Petr", duedate: '2015-10-01T00:00:00+01:00'},
        {status: "open", name: "TODO 08", owner: "Igor", duedate: '2015-10-10T00:00:00+01:00'},
        {status: "open", name: "TODO 06", owner: "Petr", duedate: '2015-10-06T00:00:00+01:00'},
        {status: "closed", name: "TODO 10", owner: "Petr", duedate: '2015-11-11T00:00:00+01:00'}
      ];
    })

    .filter("reverse", function() {

    });
