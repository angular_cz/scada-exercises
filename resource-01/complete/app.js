angular.module('orderAdministration', ['ngRoute', 'ngResource'])
  .constant('REST_URI', 'http://orders-api.angular.cz')
  .config(function ($routeProvider) {

    $routeProvider
      .when('/orders', {
        templateUrl: 'orderList.html',
        controller: 'OrderListController',
        controllerAs: 'list'
      })
      .when('/detail/:id', {
        templateUrl: 'orderDetail.html',
        controller: 'OrderDetailController',
        controllerAs: 'detail',
        resolve: {
          orderData: function (Orders, $route) {
            var id = $route.current.params.id;

            return Orders.get({'id' : id}).$promise;
          }
        }
      })
      .when('/create', {
        templateUrl: 'orderCreate.html',
        controller: 'OrderCreateController',
        controllerAs: 'create'
      })
      .otherwise('/orders');
  })
  .factory('Orders', function (REST_URI, $resource) {
    return $resource(REST_URI + '/orders/:id', {"id": "@id"});
  })
  .controller('OrderListController', function (Orders) {
    var orderCtrl = this;
    this.orders = Orders.query();

    this.statuses = {
      NEW : 'Nová',
      CANCELLED: 'Zrušená',
      PAID: 'Zaplacená',
      SENT: 'Odeslaná'
    };

    this.removeOrder = function (order) {
      order.$remove().then(function () {
        var index = orderCtrl.orders.indexOf(order);
        orderCtrl.orders.splice(index, 1);
      });
    };

    this.updateOrder = function (order) {
      order.$save();
    };

  })
  .controller('OrderDetailController', function (orderData) {
    this.order = orderData;
  })
  .controller('OrderCreateController', function ($location, Orders) {
    this.order = new Orders();

    this.save = function(order) {
      order.$save().then(function() {
        $location.path("/detail/" + order.id);
      });
    }
  });