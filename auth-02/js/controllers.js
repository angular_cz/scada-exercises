angular.module('authApp')

  .controller('OrderListController', function(Orders, ordersData) {
    var vm = this;

    this.orders = ordersData;

    this.statuses = {
      NEW: 'Nová',
      CANCELLED: 'Zrušená',
      PAID: 'Zaplacená',
      SENT: 'Odeslaná'
    };

    this.removeOrder = function(order) {
      order.$remove(function() {
        var index = vm.orders.indexOf(order);
        vm.orders.splice(index, 1);
      });
    };

    this.reloadOrder_ = function(order) {
      var index = vm.orders.indexOf(order);
      Orders.get({id: order.id}).$promise
        .then(function(originalOrder) {
          vm.orders[index] = originalOrder;
        });
    };

    this.updateOrder = function(order) {

      // TODO 5 obnovte záznam pomocí vm.reloadOrder_(order)
      order.$save();
    };
  })

  .controller('OrderDetailController', function(orderData) {
    this.order = orderData;
  })

  .controller('OrderCreateController', function(orderData, $location) {
    this.order = orderData;

    this.save = function() {
      this.order.$save(function() {
        $location.path("/orders");
      });
    };
  })

  .controller('AuthCtrl', function($scope, $modalInstance, authService) {
    this.isBadLogin = false;

    $scope.$on('restApi:loginFailed', function() {
      this.isBadLogin = true;
    }.bind(this));

    this.loginUser = function() {
      authService.login("user", "password").then(function(auth) {
        $modalInstance.close(auth);
      });
    };

    this.loginAdmin = function() {
      authService.login("operator", "password").then(function(auth) {
        $modalInstance.close(auth);
      });
    };

    this.loginFail = function() {
      authService.login("badUser", "badPassword");
    };

    this.cancel = function() {
      $modalInstance.dismiss('cancel');
    };
  })

  .controller('RejectCtrl', function($modalInstance) {
    this.cancel = function() {
      $modalInstance.dismiss('cancel');
    };
  });