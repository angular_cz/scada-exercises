'use strict';

describe('controller-03 example', function() {

  browser.get('index.html');

  it('should have initial settings', function() {
    expect(
        element(
            by.model('ctrl.user.name')
            ).getAttribute('value')
        ).toBe('Petr');

    expect(
        element(
            by.binding('ctrl.user.name')
            ).getText()
        ).toMatch('Petr');

    expect(element(by.model('ctrl.user.surname')).getAttribute('value')).toBe('Novák');
    expect(element(by.binding('ctrl.user.surname')).getText()).toBe('Příjmení: Novák');
  });

  it('should change age if model is changed', function() {
    var now = new Date();
    var expectedYear = now.getFullYear() - 1970;

    element(by.model('ctrl.user.yearOfBirth'))
        .clear()
        .sendKeys(1970)
        .then(function() {
          expect(
              element(
                  by.binding('ctrl.getAge()')
                  ).getText()
              ).toMatch(expectedYear.toString());
        });
  });

  it('should change name if model is changed', function() {
    element(by.model('ctrl.user.name'))
        .clear()
        .sendKeys('Josef')
        .then(function() {
          expect(
              element(
                  by.binding('ctrl.user.name')
                  ).getText()
              ).toMatch('Josef');
        });
  });

});